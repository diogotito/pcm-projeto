Directories:

concurrent_actors_java7 - a Maven project where we tried to suspend actors in the Quasar library
concurrent_actors_tascalate - a Maven project where we tried to use the Tascalate Javaflow library to fulfill what was asked for this project (only using `availableProcessors` threads, preventing starvation, supporting synchronous and asynchronous message sending and adding exception handling using messages)
pcm_actors - a Eclipse project where we tried to implement synchronous message sending using only Java 8 vanilla features.


