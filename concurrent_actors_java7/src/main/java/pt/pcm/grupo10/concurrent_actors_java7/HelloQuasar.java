package pt.pcm.grupo10.concurrent_actors_java7;

import co.paralleluniverse.actors.ActorRef;
import co.paralleluniverse.actors.BasicActor;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.Strand;

public class HelloQuasar {
	
	
	static class FirstActor extends BasicActor<String, String> {

		private static final long serialVersionUID = 1L;

		@Override
		protected String doRun() throws InterruptedException, SuspendExecution {
			for (;;) {
				String msg = receive();
				System.out.println("Recebi: \"" + msg + "\"");
				return msg;
			}
		}
		
	}
	
	static class SecondActor extends BasicActor<String, Void> {

		private static final long serialVersionUID = 1L;
		ActorRef<String> theFirst;

		@Override
		protected Void doRun() throws InterruptedException, SuspendExecution {
			System.out.println("vou dormir 2s");
			Strand.sleep(20);
			System.out.println("vou enviar \"Boas!\"");
			theFirst.send("Boas!");
			return null;
		}
		
		public SecondActor(ActorRef<String> theFirst) {
			super();
			this.theFirst = theFirst;
		}

	}

	public static void main(String[] args) {
		//BasicConfigurator.configure();
		FirstActor firstActor = new FirstActor();
		SecondActor secondActor = new SecondActor(firstActor.spawn());
		secondActor.spawn();
	}
	

}
