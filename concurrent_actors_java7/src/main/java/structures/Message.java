package structures;

import java.util.HashMap;
import java.util.Map;

public abstract class Message
{
	private Map<?,?> fields;
	
	public Message()
	{
		this.fields = new HashMap<Object,Object>();
	}
	
	public Map<?,?> getFields()
	{
		return this.fields;
	}
}
