package structures;

import co.paralleluniverse.actors.BasicActor;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.Strand;

public abstract class PCMActor extends BasicActor<Message, Message>
{
	private static final long serialVersionUID = 1L;

	public enum State {
		RUNNING,
		SUSPENDED,
		TERMINATED
	}

	protected State currentState = State.RUNNING;

	@Override
	protected Message doRun() throws InterruptedException, SuspendExecution
	{
		System.out.println("comecei a correr...");
		while (currentState != State.TERMINATED) {
			while (currentState == State.SUSPENDED) {
				// Acho que só é suposto chamarmos park dentro de um doRun
				System.out.println("PAREI!!!");
				Strand.park();
			}
			action();
		}
		return null;
	}
	

	protected abstract void action() throws SuspendExecution, InterruptedException;


	public void orderSuspend() throws SuspendExecution
	{
		currentState = State.SUSPENDED;
	}
	
	public void orderResume() {
		currentState = State.RUNNING;
		Strand.unpark(this.getStrand());
	}

}
