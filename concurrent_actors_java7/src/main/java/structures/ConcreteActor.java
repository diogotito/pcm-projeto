package structures;

import co.paralleluniverse.fibers.Fiber;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.Strand;

public class ConcreteActor extends PCMActor
{
	long end;
	private static final long serialVersionUID = 1L;

	int state = 0;
	
	@Override
	protected void action() throws SuspendExecution, InterruptedException
	{
		state++;
		System.out.printf("Estou a correr action() pela %3dª vez%n", state);
		Strand.sleep(100);
	}
	
	public static void main(String[] args) throws SuspendExecution, InterruptedException
	{
		new Fiber<Void>(() -> {
			ConcreteActor c = new ConcreteActor();
			c.spawn();

			int i = 0;
			while(true)
			{
				i++;
				Strand.sleep(1000);
				System.out.printf("[%3d.] vou suspender c%n", i);
				c.orderSuspend();
				Strand.sleep(1000);
				System.out.printf("[%3d:] vou acordar c%n", i);
				c.orderResume();
			}
		}).start();
		
		// mete uma thread a dormir por 5 segundos
		new Thread(() -> {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();
	}

}

