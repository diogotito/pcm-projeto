mvn clean compile

# TODO learn to configure maven exec plugin

java -cp 'javaflow.instrument-continuations.jar;maven_jars/commons-logging-1.2.jar;maven_jars/net.tascalate.javaflow.api-2.4.0.jar;maven_jars/net.tascalate.javaflow.extras-2.3.2.jar;target/classes' \
     -javaagent:javaflow.instrument-continuations.jar \
     concurrent_actors_tascalate.SimpleExample
