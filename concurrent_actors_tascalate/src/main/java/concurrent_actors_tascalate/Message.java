package concurrent_actors_tascalate;

public class Message<T>
{
	private static int numInstances = 0;

	private int id;
	private ContinuableActor from;
	private ContinuableActor to = null;
	private Message<?> replyingTo;
	private T contents;
	private boolean isSync = false;

	public Message(T contents, ContinuableActor from) {
		id = numInstances++;
		this.contents = contents;
		this.from = from;
	}
	
	public void markSync() {
		isSync = true;
	}
	
	public boolean isSync() {
		return isSync;
	}

	public int getId() {
		return id;
	}

	public ContinuableActor getFrom() {
		return from;
	}

	public T getContents() {
		return contents;
	}

	public ContinuableActor getTo() {
		return to;
	}

	public void setTo(ContinuableActor to) {
		this.to = to;
	}

	public Message<?> getReplyingTo() {
		return replyingTo;
	}

	public void setReplyingTo(Message<?> replyingTo) {
		this.replyingTo = replyingTo;
	}
	
	@Override
	public String toString() {
		return "Message [id=" + id + ", from=" + from + ", contents=" + contents + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

}
