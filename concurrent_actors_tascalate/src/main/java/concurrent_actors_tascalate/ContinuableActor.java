package concurrent_actors_tascalate;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.javaflow.api.Continuation;
import org.apache.commons.javaflow.api.continuable;

/**
 * This code will run in a continuation. The ActorScheduler class will manage the execution
 * of the following code through {@link Continuation#resume()}
 */
public class ContinuableActor implements Runnable {
	
	public static final String CHILD_ACTOR_DIED = "child actor died";
	private static final int MAILBOX_CAPACITY = 100; // TODO fetch this from config or command line arguments
	private static int actorsInstantiated = 0;
	
	// general Actor state
	public Continuation cc;
	public LinkedBlockingQueue<Message<?>> mailbox = new LinkedBlockingQueue<>(MAILBOX_CAPACITY);
	public boolean alive = true;
	public int id = actorsInstantiated++;
	
	public Set<ContinuableActor> children = new HashSet<ContinuableActor>();
	public ContinuableActor parent = null;
	
	public void addChildActor(ContinuableActor newChild) {
		children.add(newChild);
		
		if (newChild.parent != null) {
			newChild.parent.children.remove(newChild);
		}
		newChild.parent = this;
	}
	
	public void setParent(ContinuableActor newParent) {
		newParent.addChildActor(this);
	}


	@Override
	public @continuable void run() {
		try {
			
			onStart();
			while (alive) {
				// Ask the ActorScheduler to take a messsage from my mailbox from me (eventually)
				Message<?> msg = (Message<?>) Continuation.suspend(ActorScheduler.SuspendReason.TAKE_FROM_MAILBOX);
				
				// Process it
				if (msg.isSync()) {
					Message<?> syncReply = processSyncMessage(msg);
					syncReply.setReplyingTo(msg);
					msg.getFrom().send(syncReply);
				} else {
					processMessage(msg);
				}
			}
			onEnd();
			
		} catch (Exception e) {
			kill();
			if (parent != null) {
				parent.send(makeMessage(e));
			}
		} finally {
			if (parent != null) {
				parent.send(makeMessage(CHILD_ACTOR_DIED));
				parent.children.remove(this);
			}
		}
	}


	// Lifecycle hooks

	protected void onStart() {
		// Override in subclasses
	}
	
	protected void processMessage(Message<?> msg) throws Exception {
		// Override in subclasses
	}

	private Message<?> processSyncMessage(Message<?> msg) throws Exception {
		// Override in subclasses
		return makeMessage("unimplemented");
	}

	protected void onEnd() {
		// Override in subclasses
		System.err.println("An instance of " + getClass().getSimpleName() + " terminated");
	}

	protected void kill() {
		alive = false;
	}
	
	
	// Communication to this Actor
	
	public <T> void send(Message<T> msg) {
		msg.setTo(this);
		mailbox.offer(msg);
	}
	
	public @continuable Message<?> sendSync(Message<?> msg) {
		msg.markSync();
		send(msg);
		System.out.println("suspending...");
		Message<?> suspend = (Message<?>) Continuation.suspend(msg); // N�o suspende e retorna null ???
		return suspend;
	}
	
	
	// Communication to other actors
	
	public <T> Message<T> makeMessage(T content) {
		return new Message<T>(content, this);
	}


	@Override
	public String toString() {
		return "ContinuableActor [id=" + id + ", alive=" + alive + "]";
	}

}
