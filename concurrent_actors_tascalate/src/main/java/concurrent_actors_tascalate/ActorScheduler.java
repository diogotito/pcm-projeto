package concurrent_actors_tascalate;

import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.javaflow.api.Continuation;

public class ActorScheduler {
	private LinkedBlockingQueue<ContinuableActor> actorQueue = new LinkedBlockingQueue<>();
	private ActorThread[] threadPool;

	public ActorScheduler(int numThreads) {
		threadPool = new ActorThread[numThreads];
		for (int i = 0; i < numThreads; i++) {
			threadPool[i] = new ActorThread(i);
		}
	}

	public void addActor(ContinuableActor actor) {
		actorQueue.offer(actor);
	}

	public void startThreads() {
		for (int i = 0; i < threadPool.length; i++) {
			threadPool[i].start();
		}
	}

	/**
	 * Values that ContinuableActor might pass to Continuation.suspend()
	 */
	public enum SuspendReason {
		TAKE_FROM_MAILBOX
	}

	/**
	 * Hops between the execution of actors in the queue For now these threads will
	 * die when there are no more actors waiting in the queue
	 */
	/**
	 * @author diogotito
	 *
	 */
	private class ActorThread extends Thread {

		private int id;

		public ActorThread(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			ContinuableActor currentActor;

			// While there are actors in the queue waiting to keep working
			try {
				while ((currentActor = actorQueue.poll(1000, TimeUnit.MILLISECONDS)) != null) {
					System.err.println(this + " Polled " + currentActor);
					continueExecuting(currentActor);
					// Thread.sleep(100);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.err.println(this + " died because the queue is empty");
		}

		private void continueExecuting(ContinuableActor actor) {
			if (actor.cc == null && actor.alive)
			{
				// It's a newborn actor
				System.err.println("  --> " + this + " It's a newborn actor");
				actor.cc = Continuation.startWith(actor);
				System.err.println("     " + IntStream.range(0, actor.id + 1).mapToObj(n -> "_,~^-.").collect(Collectors.joining()) + "  " + actor.cc.value());
			}
			else if (actor.cc != null)
			{
				// Before we continue this actor's execution...
				// Why did it suspend its execution in the first place?
				if (actor.cc.value() instanceof Message<?> &&
					((Message<?>) actor.cc.value()).isSync())
				{
					System.err.println("  --> " + this + " you suspended because you're waiting for a sync reply");
					// The actor suspended because it is waiting for a reply for a sync message
					Message<?> awaitingReply = (Message<?>) actor.cc.value();
					
					// check if it received a reply in its mailbox...
					Optional<Message<?>> maybeReply = actor.mailbox.stream()
							                                       .filter(msg -> msg.getReplyingTo() == awaitingReply)
							                                       .findFirst();
					if (maybeReply.isPresent())
					{
						Message<?> reply = maybeReply.get();
						actor.mailbox.remove(reply);
						System.err.println("  --> " + this + " resuming because it got a reply from a sync message");
						actor.cc = actor.cc.resume(reply);
					} else {
						System.err.println("  --> " + this + " Your reply didn't arrive yet");
					}
				}
				else if (actor.cc.value() == SuspendReason.TAKE_FROM_MAILBOX)
				{
					// The actor suspended because it's going to process a new message from its mailbox
					Message<?> msg = actor.mailbox.poll();
					if (msg != null) {
						actor.cc = actor.cc.resume(msg);
						System.err.println("  --> " + this + " resuming because it got a new message in the mailbox");
					} else {
						System.err.println("  --> " + this + " Your mailbox is still empty!");
					}
				}
			}

			// Put this actor in the queue to be picked up later
			//System.err.println("  --> " + this + " see you later " + actor + "\n        (cc.value() = " + actor.cc.value() + ")");
			if (actor.alive) {
				actorQueue.offer(actor);
			}
		}

		@Override
		public String toString() {
			return "ActorThread [id=" + id + "]";
		}

	}

}
