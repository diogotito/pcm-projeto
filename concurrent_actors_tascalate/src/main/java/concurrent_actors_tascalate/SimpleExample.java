package concurrent_actors_tascalate;

import java.io.PrintStream;
import java.util.concurrent.ThreadLocalRandom;

public class SimpleExample {


	private static class ParentActor extends ContinuableActor {
		int sum = 0;
		int numbersReceived = 0;
		int numbersExpecting = 0;
		
		@Override
		protected void onStart() {
			for (ContinuableActor child : children) {
				if (child instanceof ChildActor) {
					child.send(makeMessage("Give me a number for me to add"));
					numbersExpecting++;
				} else {
					child.send(makeMessage("Do whatever you want"));
				}
			}
		}
		
		@Override
		protected void processMessage(Message<?> msg) {
			if (msg.getContents() instanceof Integer) {
				System.out.println("Received " + msg.getContents() + " from one of my children");
				sum += (Integer) msg.getContents();
				numbersReceived++;
				if (numbersReceived >= numbersExpecting) {
					System.out.println("The sum is " + sum);
				}
			} else if (msg.getContents() instanceof Exception) {
				System.out.println("I received an exception from " + msg.getFrom() + " and it says \"" +
						((Exception) msg.getContents()).getMessage() + "\"");
			} else if (ContinuableActor.CHILD_ACTOR_DIED.equals(msg.getContents())) {
				System.out.println(children.size() + " children alive");
				if (children.isEmpty()) {
					kill();
				}
			}
		}
	}
	
	
	private static class ChildActor extends ContinuableActor {
		@Override
		protected void processMessage(Message<?> msg) {
			msg.getFrom().send(makeMessage(ThreadLocalRandom.current().nextInt(0, 100)));
			kill();
		}
	}
	
	
	private static class FaultyActor extends ContinuableActor {
		@Override
		protected void processMessage(Message<?> msg) throws Exception {
			kill();
			throw new Exception("Take this!");
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		ActorScheduler scheduler = new ActorScheduler(Runtime.getRuntime().availableProcessors());
		
		ParentActor adder = new ParentActor();
		scheduler.addActor(adder);
		ChildActor[] generators = new ChildActor[4];
		for (int i = 0; i < generators.length; i++) {
			generators[i] = new ChildActor();
			adder.addChildActor(generators[i]);
			scheduler.addActor(generators[i]);
		}
		FaultyActor throwsException = new FaultyActor();
		adder.addChildActor(throwsException);
		scheduler.addActor(throwsException);
		
		System.setErr(new PrintStream("log.txt"));
		scheduler.startThreads();
	}

}
