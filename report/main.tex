\documentclass[runningheads]{llncs}

\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{lipsum} % temporario

\begin{document}

% \thanks{Supported by organization x.}
\title{Concurrent Actors}
\author{António Dias\inst{1} \and
Diogo Marques\inst{1}
}
%\authorrunning{F. Author et al.}

\institute{Departamento de Informática da Faculdade de Ciências da Universidade de Lisboa
\email{\{fc47811,fc47847\}@alunos.fc.ul.pt}}

\maketitle

\begin{abstract}
The Actor Model constitutes one of the most well-established design paradigms for systems that make use of sequential operations, parallelized processing over a global data set, or some combination of these. One of the model’s main attractions is its intrinsic capability for asynchronous message-passing between processes; after all, it is always possible to separate and parallelize the three main operations that, essentially, make up for most of an actor’s life cycle operations: taking a message from its mailbox, processing said message according to the specified implementation and dispatching the output to its next actor(s). The model also has the advantage of permitting state sharing between actors without the need for cumbersome locking mechanisms. Extending the actor model to allow for synchronous communication comes at a price: threads should be made to wait for one or more replies before continuing their execution flow. These halting states, however, are hard for the JVM to pinpoint and reassign a priority to, meaning any CPU time optimization should be done manually on the proper points of code. This can be achieved through both continuations and callbacks. Continuations allow for a program to be suspended and have its state stored to be resumed later on, and are unsupported by Java as of version 11, whereas callbacks have been used in a wide variety of manners over the years – most notably on reactive GUIs and Observer pattern implementations. This paper will discuss the actor model and explore both approaches in the context of synchronous communication.

\keywords{Synchronous communication  \and Actor Model \and Java keyword.}
\end{abstract}
%
%
%
\section{Introduction}
%(história\ldots{})
The \emph{Actor Model}, as suggested by its name, is
based upon the concept of a computational actor. Generally speaking, an
actor is a type of process that recursively follows the behavioural
rules listed below:

\begin{itemize}
\item
  An actor is a concurrent process that operates over ``messages'' whose
  format can be completely defined by the specific implementation of the
  model.
\item
  Messages are passed between actors by means of their individual
  ``mailboxes'' -- alternatively, data sets - on which messages are
  deposited from one actor to the next.
\item
  Actors have references to its subsquent actors' mailboxes. An actor,
  however, doesn't (and shouldn't) ``know'' the sender of an incoming
  message. After all, low coupling is one of the model's main hallmarks.
\end{itemize}

Thus, the general process flow of an actor can be resumed in three
distinct operations:

\begin{enumerate}
\def\labelenumi{\arabic{enumi})}
\item
  Take a message from its mailbox,
\item
  Process the message, and
\item
  Send the message to its next actors.
\end{enumerate}

\includegraphics[width=\textwidth]{media/image1.jpeg}

It becomes almost trivial to see exactly how the actor model allows for
asynchronous communication: the aforementioned ``mailbox'' can be either
unbounded or bounded in size to a specific value which, when greater
than 1, opens up the possibility of a message being processed long after
it was received. (this sort of communication is widely used on
\emph{email} applications and high-performance data transfer protocols
such as the UDP). Since, at any given point, there are no two actors
sharing state or holding references to the same objects\footnote{One
  could imagine the exceptional case of two actors simultaneously
  pushing messages to the same mailbox. Thankfully, Java supports a wide
  variety of thread-safe data sets -- such as the
  ConcurrentLinkedQueue\textless{}T\textgreater{} -- that can be used to
  implement an actor model mailbox.}, nor performing blocking tasks, the
\emph{Actor Model} allows for non-locking concurrence. Synchronous
communication, however, is desirable -- if not downright necesssary --
on some particular computational models like those found on systems of
the RPC type.
The objectives set for this project are as follows:
\begin{itemize}
\item To enhance the actor model to allow for both synchronous and asynchronous communication between actors;
\item To implement continuations on the Java language, and to subsequently use them on the actor model.
\end{itemize}

\section{Approach}
When thinking about synchronous message-passing between actors, one's
first idea would, most likely, involve some sort of mechanism that
forces an actor to halt execution until it receives the reply -- or
replies -- it needs to proceed. In Java, this could be accomplished by
adding a busy-loop that exits when the required replies are received and
read from the mailbox:

\begin{lstlisting}[language=Java]
        int numWaiting = 0;
        for(Actor a : nextActors)
        {
        	m.setOrigin(confirmations);
        	a.addMessage(m);
        	numWaiting++;
        }
        suspended = true;
        hl{while(confirmations.size() < numWaiting) {}
        for(Message mess : confirmations)
        {
        	processMessage(mess);
        }
        Set<Message> replies = new HashSet<Message>
                                        (confirmations);
        confirmations.clear();
        suspended = false;
        return replies;
\end{lstlisting}

(Keep in mind the above algorithm was developed as a simplistic test;
most real-world applications would likely involve some sort of
pattern-matching with the inbound replies and a more complex locking
mechanism.)

While the presented solution solves the problem of synchronous communication
on a typical actor model implementation, it also brings a significant
handicap to the table: the actor on such a rudimentary ``halting'' state
isn't doing any useful work. This is exarcebated by the fact that, for
the JVM, there isn't any fundamental difference between a working thread
and a halted thread (from now onwards, ``halted'' means as was defined
previously). Aditionally, there isn't a simple, direct way to instruct
the thread scheduler to take into consideration halted actors; JVM
threads are mapped directly to the native operation system threads that
are scheduled by the OS itself.

This hindrance can be partially solved through the use of Continuations.
A Continuation is an object representing a particular state of a
program, and can be used to resume execution of previously-suspended
threads. While Continuations aren't officially supported by the Java
language at the time of this report, there's a wide variety of libraries
which, in way or another, allow for a piece of code to be suspended and
resumed later on.

\subsection{Quasar}

The Quasar\cite{quasar} library contains a set of tools that allow for
high-performance threading, including a lightweight thread framework, an
efficient scheduler and a custom-made actor model. The library's
flagship feature is the Fiber object. A Fiber is a thread optimized for
operations that block frequently and, through the internal use of
Continuations, allows for the fast swapping of jobs via the
aforementioned scheduler. It should also be mentioned Fibers are
remarkable in their low memory footprint; according to main site, idle
fibers occupy about 400 bytes of RAM. Compatibility between Fibers and
legacy Threads is accomplished through an additional level of
abstraction realized in the Strand superclass, which is extended by both
classes.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{media/image2.jpeg}
  \caption{Type hierarchy of the Fiber and FiberScheduler classes} \label{fig:fiber} 
\end{figure}

Quasar also includes its own implementation of the actor model. The
Actor class is an execution unit that runs on a Strand and is capable of
receiving messages from other processes through its Mailbox object.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{media/image3.jpeg}
  \caption{Class-level relationships of the Quasar Actor implementation} \label{fig:qactor} 
\end{figure}

As it happens with many Continuation libraries, thread halting is
implemented by means of bytecode instrumentation. Methods whose
execution can be suspended (and are identified as such by either a
throws (SuspendExecution se) clause or with the @Suspendable notation\footnote{The "exception" mechanism is simply a way to signal a method's suspendability; ultimately, such exceptions aren't treated and aren't allowed to be catched by the Quasar instrumentation.})
have their own code enhanced by calls to the park/unpark methods before
and after calls to other suspendable methods, respectively\footnote{As
  it happens with Strand.park and Strand.unpark, the Strand.yield()
  method works similarly to the legacy Thread.yield(): while it marks a
  thread's intention to give up its current use of a processor, it might
  not result in immediate surrender of CPU time.}. Thus, it becomes
clear Continuations are implemented internally and, as advised on the
Quasar website, are not meant to be employed directly in user
development.

In the context of this assignment, it was decided the Quasar library
wasn't adequate for the final delivered program. This was due to two
main factors:

\begin{enumerate}
\def\labelenumi{\arabic{enumi})}
\item
  Much of the required work and objectives could be easily realized by
  simply using the provided framework. During the time the group worked
  with the Quasar library, it became increasingly hard to find ways to
  contribute to the actor model topic without entering a pitfall of
  over-reliance on pre-made code.
\item
  Since the purpose of the project was to obtain insight into the
  synchronous communication, actor model and Continuation paradigms, it
  was understood the Quasar library's complex type hierarchy and
  functionalities would be inedequate for investigative work. Much of
  the scheduling is done internally, and the group wasn't able to, for
  instance, develop a straighforward way to directly suspend and resume
  the execution of an actor.
\end{enumerate}

Nevertheless, the study of the Quasar library provided us with valuable
knowledge which was carried over to the next -- and final -- adopted
strategy.



\subsection{JavaFlow}
After trying Quasar, we decided to build an actor system on top of the Continuations provided by the
Apache Jakarta-Commons JavaFlow\cite{javaflow} library. As this project was more than a decade old and seemed to be
abandoned, we ended up using the Tascalate JavaFlow project that offered us the same Continuations
rewritten to support more recent versions of the Java language.

As JavaFlow gave us direct access to control our Continuations, we were able to more easily implement
our own actor scheduling logic, which we ended up implementing as follows:

\begin{enumerate}
\def\labelenumi{\arabic{enumi})}
\item
  An \texttt{ActorScheduler} initializes a pool of $N$ \texttt{ActorThead}s (where $N$ is the number of
  available processors on the machine where the JVM is running).
\item
  The actors (which are instances of a subclass of \texttt{ContinuableActor})
  are put in a queue that is polled by the $N$ \texttt{ActorThead}s.
  As the actors are polled, the \texttt{ActorThread}s execute the actors' code through the methods
  \texttt{Continuation.startWith} and \texttt{Continuation.resume},
  which allow the actors to call \texttt{Continuation.suspend} with a value to hand
  back the control to the \texttt{ActorThread}, which in turn puts the actor back to the end of the queue
  to be picked up later by another \texttt{ActorThread}. The Continuation objects that result from this
  suspension are stored in the actors so that it is easier for different \texttt{ActorThread}s to resume the execution of an actor.
\item
  Before an actor's execution is resumed, the thread that polled it checks the value that was passed to
  \texttt{Continuation.suspend}, which is stored in the actor's \texttt{Continuation} object.
  An actor will either
  \begin{itemize}
      \item
        have passed a \texttt{SuspendReason.TAKE\_FROM\_MAILBOX} to ask the next thread that picks it up to
        only resume its execution if there are messages in its mailbox to be processed.
        In that case, the thread should
        take a message from actor's mailbox and pass it to the actor through \texttt{Continuation.resume}
      \item
        have passed a synchronous \texttt{Message} to signal the scheduler that the actor is waiting to receive
        a reply for that message. A thread should only resume the actor's execution when such reply is
        found on the actor's mailbox, in which case the thread should take it from the mailbox and hand it
        over to the actor by passing it to \texttt{Continuation.resume}.
  \end{itemize}
  
\end{enumerate}

We managed to successfully use the JavaFlow library to make the actors completely idle
while their mailbox is empty.
However, there was a problem in this library that didn't allow our synchronous message passing mechanism
to work.
More specifically, a call to \texttt{Continuation.suspend} acted as a \texttt{return null} statement instead
of jumping back to the \texttt{ActorThread} that resumed it.



% \section{Implementation Details}
% 
% In this section you can talk about details of how you implemented your approach. Depending on the detail of your % approach, this might be an optional section.
% 
% As an example, I would detail how I implemented the phaser in the genetic algorithm, or how I implemented % parallel mergesort on ForkJoin. Another aspect could be how to organize your arrays to minimize overhead in % recursive calls.
% 
% 
% \section{Evaluation}
% 
% \subsection{Experimental Setup}
% 
% In this section you should describe the machine(s) in which you are going to evaluate your system. Select the % information that is relevant.
% 
% 
% \subsection{Results}
% 
% In this section you should present the results. Do not forget to explain where the data came from. 
% 
% You should include (ideally vectorial) plots, with a descriptive caption. Make sure all the plots (Like % Figure~\ref{fig1} are well identified and axis and metrics are defined.
% 
% \begin{figure}[htbp]
% \includegraphics[width=\textwidth]{code/performance.eps}
% \caption{Comparison of the performance of sequential and parallel versions of the algorithm.} \label{fig1}
% \end{figure}

\section{Conclusions}

The objectives set by the group - and mentioned on the introduction of this paper - were accomplished in one way or another: 

\begin{itemize}
\item Synchronized communication can be easily achieved in Java, either by  callbacks or more rudimentary approaches such as the aformentioned busy-looping. As for its role on actor-based systems, it could be argued the use of synchronization would rarely be desirable. The model's driving feature is its absence of locking mechanisms, which translates into simpler development and debugging of the employed communication system. To move the point further, a set of synchronized actors would behave very similarly to any typical call-return system, where other architectures such as routine-subroutine modules would be more adequate.
\item
  The direct use of Continuations, as provided by projects like JavaFlow, isn't very recommended because it was understood they haven't achieved an acceptable level of maturation, as witnessed during our attempt to implement our own synchronous message passing
  mechanism with these libraries; despite these difficulties,
  % implementation of Continuations was achieved by the group. Additionally,
  we were able to spread the actors'
  execution to different threads, which greatly simplified the logic of
  % giving all of them a fair share of CPU time.
  scheduling their execution with ourselves.
\end{itemize}

This project, during its lifecycle, gradually began assuming a more investigative nature. The relative inexperience the members of the group had with the existing Java continuations implementations led to most of the time being spent learning the inner-workings of those libraries which, as was understood by us, was extremely valuable in terms of acquired knowledge.

\subsection{Future work}

Future work on the topic of Java continuations would, most likely, make use of Project Loom\cite{pLoom}, an ongoing OpenJDK library which is implementing continuations, fibers and other lightweight threading mechanisms, and is to be included in JDK12.

The scheduling of suspendable actors would also present a different challenge. While scheduling is handled on the OS level, it is possible to assign threads a priority which could depend on the time said thread spent suspended, its mailbox size, and quite possible the number of pending replies in case synchronized communication is used. While the delivered \texttt{BimodalScheduler} class wasn't tested with \texttt{ContinuableActors}, it nevertheless presents a solid idea of how such a scheduler would work: actors would be placed on a \texttt{PriorityQueue} which, besides being sorted in-place with each \texttt{add()} operation, allows the user to define those sorting criteria via a \texttt{Comparator} object. Additionally, this approach could be easily integrated with the existing logic of the \texttt{ActorThread}s polling this queue to get the next actor to resume its execution.

\section*{Acknowledgements}

António Dias wrote the proof-of-concept \texttt{BimodalScheduler} class and provided the Assignment 3's Java Actor Model as a baseline for further developments. Diogo Tito was responsible for implementing Continuations through the Tascalate library, and was responsible for much of the later investigative work. 

As for the paper, António Dias wrote the abstract, introduction and Quasar sections, whereas Diogo Tito focused on the Tascalate and Javaflow libraries.

Other sections, such as the conclusion, were divided between both.

Each author spent around 35 hours on this project.

\bibliographystyle{splncs04}
\bibliography{bibliography}

\end{document}
