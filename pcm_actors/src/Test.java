import pcm_actors.Actor;
import pcm_actors.Message;
import scheduler.BimodalScheduler;
import scheduler.Mode;

public class Test
{

	public static void main(String[] args)
	{
		final Actor ane = new Actor("Ane") {
			@Override
			protected void setup()
			{
				System.out.println("Eu sou a ane e estou a correr na thread " +
						Thread.currentThread().getName() + "|" + this.name);
			}

			@Override
			protected void processMessage(Message msg)
			{

			}

			@Override
			protected Message processMessageWithCallback(Message msg)
			{
				System.out.println("ane - recebi: " + msg.contents);
				return makeMessage("a minha resposta");
			}

		};

		Actor bob = new Actor("Bob") {
			@Override
			protected void setup()
			{
				System.out.println("Eu sou o bob e estou a correr na thread " +
						Thread.currentThread().getName() + "|" + this.name);
				ane.sendMessageSync(makeMessage("Ol�"), (Message response) -> {
					System.out.println("bob - Recebi: " + response.contents);
				});
			}

			@Override
			protected void processMessage(Message msg)
			{
				// TODO Auto-generated method stub

			}

			@Override
			protected Message processMessageWithCallback(Message msg)
			{
				// TODO Auto-generated method stub
				return null;
			}
		};

		
//		BimodalScheduler scheduler = new BimodalScheduler(10, Mode.QUEUE_SIZE);
//		scheduler.scheduleActor(ane);
//		scheduler.scheduleActor(bob);
		
		BimodalScheduler bs = new BimodalScheduler(10, Mode.SUSPENDED, new int[]{100,200,50});
		Actor a;
		RootActor ra = new RootActor("coiso");
		int maxCores = Runtime.getRuntime().availableProcessors();
		System.out.println(args.length);
		for(int i = 0; i < /* maxCores*2 */ (args.length == 1 ? Integer.parseInt(args[0]) : 4); i++)
		{
			a = new ChildActor("Actor " + i); 
			ra.addActor(a);
			bs.scheduleActor(a);
		}		
		bs.scheduleActor(ra);
		bs.run();
		Message kickoff = new Message("0,100", null);
		ra.sendMessage(kickoff);
		
		while(ra.alive()) {}
		bs.kill();
	}
}
