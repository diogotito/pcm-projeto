package scheduler;

import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

import pcm_actors.Actor;

public class BimodalScheduler
{		 
    private ExecutorService priorityJobPoolExecutor;
    private ExecutorService priorityJobScheduler = Executors.newSingleThreadExecutor();
	private PriorityBlockingQueue<Actor> priorityQueue;
	private final int N_CORES = Runtime.getRuntime().availableProcessors();
	private double[] weights = new double[3];
	private boolean alive = true;

	public BimodalScheduler(Integer queueSize, Mode mode, int[] weight) 
	{
	    priorityJobPoolExecutor = Executors.newFixedThreadPool(N_CORES);
	    priorityQueue = new PriorityBlockingQueue<Actor>(queueSize, mode == Mode.QUEUE_SIZE ?
	    				queueSizeComparator : 
	    				suspendedComparator);
	    setWeights(weight);
	}
	
	public void run()
	{
		priorityJobScheduler.execute(() -> 
	    {
	    	Actor a;
	        while (alive) 
	        {
	            //System.out.println("Vou tirar da priorityQueue!");
				if(priorityQueue.peek() != null)
				{
					a = priorityQueue.poll();
					priorityJobPoolExecutor.execute(a);
				}
	        }
	        System.out.println("coiso");
	    });
	}
	
	Comparator<Actor> queueSizeComparator = new Comparator<Actor>()
	{
		public int compare(Actor actor1, Actor actor2) 
		{
	        return actor1.getMailboxSize() - actor2.getMailboxSize() == 0 ?
	        		1 : actor1.getMailboxSize() - actor2.getMailboxSize();
	    }
	};
	
	Comparator<Actor> suspendedComparator = new Comparator<Actor>()
	{
		public int compare(Actor actor1, Actor actor2) 
		{
	        boolean is1Suspended = actor1.isSuspended();
	        boolean is2Suspended = actor2.isSuspended();
	        if(is1Suspended && is2Suspended)
	        	return (int) (actor1.getSuspendedTime() - actor2.getSuspendedTime());
	        else if (!is1Suspended && !is2Suspended)
	        	return actor1.getMailboxSize() - actor2.getMailboxSize();
	        else
	        {
	        	return smartScheduler(actor1, actor2);
	        }
	    }
	};

	public void scheduleActor(Actor actor) 
	{
		priorityQueue.add(actor);
	}
	
	public void setWeights(int[] weights)
	{
		int total = Arrays.stream(weights).sum();
		int timeW = weights[0];
		int mboxW = weights[1];
		int callW = weights[2];
		
		this.weights[0] = timeW / total;
		this.weights[1] = mboxW / total;
		this.weights[2] = callW / total;
	}
	
	public int smartScheduler(Actor a1, Actor a2)
	{
		double A1_SCORE, A2_SCORE;
		
		long A1_time = a1.getSuspendedTime();
		long A2_time = a2.getSuspendedTime();
		long totalTime = A1_time + A2_time;
		if(totalTime == 0) totalTime = Long.MAX_VALUE;
		
		long A1_mbox = a1.getMailboxSize();
		long A2_mbox = a2.getMailboxSize();
		long totalMBox = A1_mbox + A2_mbox;
		if(totalMBox == 0) totalMBox = Long.MAX_VALUE;
		
		long A1_call = a1.getCallbackSize();
		long A2_call = a2.getCallbackSize();
		long totalCall = A1_call + A2_call;
		if(totalCall == 0) totalCall = Long.MAX_VALUE;
		
		A1_SCORE = (A1_time/totalTime)*weights[0] + (A1_mbox/totalMBox)*weights[1] + (A1_call/totalCall)*weights[2];
		A2_SCORE = (A2_time/totalTime)*weights[0] + (A2_mbox/totalMBox)*weights[1] + (A2_call/totalCall)*weights[2];
		
		return Double.compare(A1_SCORE, A2_SCORE);
	}
	
	public void kill()
	{
		alive = false;
	}
}
