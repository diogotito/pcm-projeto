package pcm_actors;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

public abstract class Actor extends Thread
{
	private boolean alive = true;
	private LinkedBlockingQueue<Message> mailbox = new LinkedBlockingQueue<>();
	private LinkedBlockingQueue<Runnable> callbacksToRun = new LinkedBlockingQueue<>();
	private long suspendedTime;

	protected String name;
	
	protected boolean suspended = true;
	
	public Actor(String name) {
		this.name = name;
		setName(name + "'s Thread");
	}

	
	@Override
	public final void run()
	{
		setup();
		suspended = true;
		while (alive) {
			if (callbacksToRun.peek() != null) {
				suspended = false;
				callbacksToRun.poll().run();
				suspended = true;
			}

			if (mailbox.peek() != null) {
				suspended = false;
				Message next = mailbox.poll();
				if (next.callback != null) {
					Message response = processMessageWithCallback(next);
					try {
						next.from.enqueueCallback(next.callback, response);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					processMessage(next);
				}
				suspended = true;
			}
		}
		onEnd();
	}

	protected void setup()
	{
		
	}

	protected void onEnd()
	{
		
	}

	abstract protected void processMessage(Message msg);

	abstract protected Message processMessageWithCallback(Message msg);
	

	public void terminate() {
		alive = false;
	}
	
	public Message makeMessage(String contents) {
		return new Message(contents, this);
	}
	
	public void sendMessage(Message msg) {
		try {
			mailbox.put(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void sendMessageSync(Message msg, Consumer<Message> callback) {
		msg.callback = callback;
		sendMessage(msg);
	}
	
	
	public void enqueueCallback(Consumer<Message> callback, Message response) throws InterruptedException {
		System.out.println("Enqueuing callback on thread " + Thread.currentThread().getId() + "|" + Thread.currentThread().getName());
		callbacksToRun.put(() -> {
			System.out.println("Running curried callback on thread " + Thread.currentThread().getId() + "|" + Thread.currentThread().getName());
			callback.accept(response);
		});
	}

	public int getMailboxSize()
	{
		return mailbox.size();
	}

	public int getCallbackSize()
	{
		return callbacksToRun.size();
	}
	
	public long getSuspendedTime()
	{
		return System.nanoTime() - suspendedTime;
	}
	
	public boolean isSuspended()
	{
		return suspended;
	}
	
	public void suspendActor()
	{
		suspended = true;
		suspendedTime = System.nanoTime();
	}
	
	public boolean alive()
	{
		return alive;
	}
	
	public void resumeActor()
	{
		suspended = false;
		suspendedTime = -1;
	}
	
	public String toString()
	{
		return name;
	}
}
