package pcm_actors;

import java.util.function.Consumer;

public class Message
{
	private static int numInstances = 0;
	
	private int id;
	
	public Consumer<Message> callback = null;

	public Actor from;

	public String contents;

	public Message() {
		id = numInstances++;
	}
	
	public Message(String c, Actor from) {
		contents = c;
		this.from = from;
	}
	
}
