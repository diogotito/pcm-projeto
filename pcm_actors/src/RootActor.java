import java.util.ArrayList;

import pcm_actors.Actor;
import pcm_actors.Message;

public class RootActor extends Actor
{
	private int sum = 0;
	private int nReceived = 0;
	
	private ArrayList<Actor> nextActors = new ArrayList<Actor>();
	
	public RootActor(String name) {
		super(name);
	}

	@Override
	protected void processMessage(Message msg)
	{
		String message = msg.contents;
		if(message.contains("Result"))
		{
			System.out.println("Recebi resposta do actor " + msg.from.toString() + " !");
			nReceived++;
			sum += Integer.parseInt(message.split(":")[1]);
			if(nReceived == nextActors.size())
			{
				System.out.println("Resultado: " + sum);
				// terminate();
				System.exit(0);
			}
		}
		else
		{
			// format: "start,end"
			String[] s = msg.contents.split(",");
			Message m;
			int start = Integer.parseInt(s[0]);
			int end = Integer.parseInt(s[1]);
			int nChildren = nextActors.size();
			int partition = (end-start)/nChildren, startHere = 0, endHere = 0;
	
			for(int i = 0; i < nextActors.size();i++)
			{
				startHere = i*partition;
				endHere = startHere + partition - 1;
				m = makeMessage(startHere + "," + endHere);
				nextActors.get(i).sendMessage(m);
			}
		}
	}
	
	@Override
	protected void onEnd()
	{
		for(Actor a : nextActors)
		{
			a.terminate();
		}
	}

	@Override
	protected Message processMessageWithCallback(Message msg)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	public void addActor(Actor a)
	{
		nextActors.add(a);
	}

}
