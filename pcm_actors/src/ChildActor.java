import pcm_actors.Actor;
import pcm_actors.Message;

public class ChildActor extends Actor
{

	public ChildActor(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void processMessage(Message msg)
	{
		System.out.println("Recebi pedido da raiz!");
		// TODO Auto-generated method stub
		String[] parameters = msg.contents.split(",");
		int start = Integer.parseInt(parameters[0]);
		int end = Integer.parseInt(parameters[1]);
		int count = 0;
		
		for(int i = start; i < end; i++)
		{
			count += i;
		}
		
		Message result = makeMessage("Result:" + count);
		msg.from.sendMessage(result);
		terminate();		
	}

	@Override
	protected Message processMessageWithCallback(Message msg)
	{
		return msg;
		
	}

}
